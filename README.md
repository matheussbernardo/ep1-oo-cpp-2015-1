1 - Entre na pasata raiz do projeto pelo terminal

2 - Digite o comando
	
	make

3 - Entre na pasta bin/ e execute o finalBinary com os comandos
	
	cd bin/
	./finalBinary

A imagem a ser lida tem que ficar junto a pasta raiz do projeto

Escolha uma opção e uma outra imagem vai ser gerada também na pasta raiz do projeto.

Caso queira, execute o comando ./finalBinary novamente e teste outros filtros.
