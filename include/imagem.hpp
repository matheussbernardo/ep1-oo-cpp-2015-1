#ifndef IMAGEM_H
#define IMAGEM_H

#include <iostream>
#include <string>
#include <fstream>
#include <sstream>
#include <stdlib.h> 

using namespace std;

class Imagem {
	private:
		int linhas;
		int colunas;
		int cinza;
		int *matriz;
	public:
	
		Imagem();
		Imagem(const char fname[]);
		Imagem(int linhas, int colunas, int cinza);
		~Imagem();
		int getLinhas();
		int	getColunas();
		int getCinza();
		void setLinhas(int linhas);
		void setColunas(int colunas);
		void setCinza(int cinza);
		
		int getValordeUmPixel(int linha, int coluna);
		void setValordeUmPixel(int linha, int coluna, int valor);
		
		
		void lerImagem(const char fname[]);
		void salvarImagem(const char filename[]);		
};




#endif
