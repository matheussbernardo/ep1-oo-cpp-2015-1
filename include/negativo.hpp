#ifndef NEGATIVO_H
#define NEGATIVO_H

#include <iostream>
#include <string>
#include <fstream>
#include <sstream>
#include <stdlib.h> 

#include "filtro.hpp"
#include "imagem.hpp"

class Negativo : public Filtro {
	public:
		Negativo();
		
		void aplica(Imagem &umaImagem);
		
};

#endif
