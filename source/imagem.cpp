#include "imagem.hpp"

using namespace std;

Imagem::Imagem() {
	linhas = 0;
	colunas = 0;
	cinza = 0;

	matriz = NULL;
}
Imagem::Imagem(const char fname[]){
	char header [100], *ptr;
    ifstream ifp;
	
	ifp.open(fname, ios::in | ios::binary);

    if (!ifp) 
    {
        cout << "Can't read image: " << fname << endl;
        exit(1);
    }


    ifp.getline(header,100,'\n');
    if ( (header[0]!=80) || (header[1]!=53) ) // P5
    {   
        cout << "Image " << fname << " is not PGM" << endl;
        exit(1);
    }

    ifp.getline(header,100,'\n');
    while(header[0]=='#')
        ifp.getline(header,100,'\n');

    linhas=strtol(header,&ptr,0);
    colunas=atoi(ptr);

    ifp.getline(header,100,'\n');
    cinza=strtol(header,&ptr,0);
    matriz = new int[linhas*colunas];
}
Imagem::Imagem(int linhas, int colunas, int cinza) {
	this->linhas = linhas;
	this->colunas = colunas;
	this->cinza = cinza;
	matriz = new int[linhas*colunas];
}
Imagem::~Imagem(){
	delete []matriz;
}

int Imagem::getLinhas() {
	return linhas;	
}

int Imagem::getColunas() {
	return colunas;
}

int Imagem::getCinza() {
	return cinza;
}

void Imagem::setLinhas(int linhas) {
	this->linhas=linhas;	
}

void Imagem::setColunas(int colunas) {
	this->colunas = colunas;
}

void Imagem::setCinza(int cinza) {
	this->cinza = cinza;
}

void Imagem::lerImagem(const char fname[]) {
	int i, j;
    unsigned char *charImage;
    char header [100], *ptr;
    ifstream ifp;

    ifp.open(fname, ios::in | ios::binary);

    if (!ifp) 
    {
        cout << "Can't read image: " << fname << endl;
        exit(1);
    }

 // read header

    ifp.getline(header,100,'\n');
    if ( (header[0]!=80) || (header[1]!=53) ) // P5
    {   
        cout << "Image " << fname << " is not PGM" << endl;
        exit(1);
    }

    ifp.getline(header,100,'\n');
    while(header[0]=='#')
        ifp.getline(header,100,'\n');

    linhas=strtol(header,&ptr,0);
    colunas=atoi(ptr);

    ifp.getline(header,100,'\n');
    cinza=strtol(header,&ptr,0);

    charImage = (unsigned char *) new unsigned char [linhas*colunas];

    ifp.read( reinterpret_cast<char *>(charImage), (linhas*colunas)*sizeof(unsigned char));

    if (ifp.fail()) 
    {
        cout << "Image " << fname << " has wrong size" << endl;
        exit(1);
    }

    ifp.close();

 //
 // Convert the unsigned characters to integers
 //

    int val;

    for(i=0; i<linhas; i++)
        for(j=0; j<colunas; j++) 
        {
            val = (int)charImage[i*colunas+j];
            matriz[i*colunas+j] = val;   
        }

    delete [] charImage;
}

void Imagem::salvarImagem(const char filename[]) {
	int i, j;
	unsigned char *charImage;
	ofstream outfile(filename);
	
	charImage = (unsigned char *) new unsigned char [linhas*colunas];
	
	int val;

    for(i=0; i<linhas; i++)
    {
        for(j=0; j<colunas; j++) 
        {
			// ary[i][j] is then rewritten as
			//ary[i*sizeY+j]
			val = matriz[i*colunas+j];
			charImage[i*colunas+j]=(unsigned char)val;
        }
    }
    
	if (!outfile.is_open())
	{
		cout << "Can't open output file"  << filename << endl;
		exit(1);
	}
	
	outfile << "P5" << endl;
    outfile << linhas << " " << colunas << endl;
    outfile << 255 << endl;
	
	//pixels
	outfile.write(reinterpret_cast<char *>(charImage), (linhas*colunas)*sizeof(unsigned char));
	
	cout << "Arquivo gerado" << endl;
	outfile.close();
}
//retorna o valor de cinza de um pixel especifico
int Imagem::getValordeUmPixel(int linha, int coluna)
{
	int sizeY = getColunas();
    return matriz[linha*sizeY+coluna];
}

//seta o valor de cinza de um pixel especifico
void Imagem::setValordeUmPixel(int linha, int coluna, int valor)
{
	int sizeY = getColunas();
	
    matriz[linha*sizeY+coluna] = valor;
}



