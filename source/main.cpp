#include "imagem.hpp"
#include "filtro.hpp"
#include "negativo.hpp"
#include "sharpen.hpp"
#include "smooth.hpp"
using namespace std;

int main() {
	int opcao;
	
	Negativo umNegativo;
	Smooth umSmooth;
	Sharpen umSharpen;

	Imagem umaImagem("../lena.pgm");
	
	umaImagem.lerImagem("../lena.pgm");
	
	
	cout << "A imagem foi lida com sucesso, escolha o filtro:" << endl;
	cout << "1 - Negativo" << endl << "2 - Smooth" << endl << "3 - Sharpen" << endl;
	
	cout << "Opcao: ";
	cin >> opcao;
	if(opcao == 1) {
		umNegativo.aplica(umaImagem);
		umaImagem.salvarImagem("../lenaNegativo.pgm");
	}
	if(opcao == 2) {		
		umSmooth.setDiv(9);
		umSmooth.setSize(3);
		umSmooth.aplica(umaImagem,umSmooth.getDiv(), umSmooth.getSize());
		umaImagem.salvarImagem("../lenaSmooth.pgm");
	}
	if(opcao == 3) {
		umSharpen.setDiv(1);
		umSharpen.setSize(3);
		umSharpen.aplica(umaImagem, umSharpen.getDiv(), umSharpen.getSize());
		umaImagem.salvarImagem("../lenaSharpen.pgm");
	}
	else
		return 0;
	
		
	return 0;
}
