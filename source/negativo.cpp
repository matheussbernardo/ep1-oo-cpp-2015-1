#include "negativo.hpp"
#include "filtro.hpp"

using namespace std;

Negativo::Negativo(){	
	
}

void Negativo::aplica(Imagem &umaImagem) {
		
		int linhas, colunas;
		
		linhas = umaImagem.getLinhas();
		colunas = umaImagem.getColunas();
		
		for (int i=0; i < linhas; i++){
			for (int j = 0; j < colunas; j++) {
				umaImagem.setValordeUmPixel(i,j, 255-umaImagem.getValordeUmPixel(i, j) );
			}
		} 

}
