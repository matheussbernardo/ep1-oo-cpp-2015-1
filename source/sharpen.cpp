#include "sharpen.hpp"
#include "filtro.hpp"

using namespace std;

Sharpen::Sharpen(){	
	
}

void Sharpen::aplica(Imagem &umaImagem, int div, int size) {
		int sharpen[] = {0, -1, 0, -1, 5, -1, 0, -1, 0};

		int linhas, colunas, valor, i,j;
		
		
	
		linhas = umaImagem.getLinhas();
		colunas = umaImagem.getColunas();
		int *m = new int[linhas*colunas];
		
		for (i=size/2; i < linhas-size/2; i++)
		{
			for (j = size/2; j < colunas-size/2; j++)
			{
				valor = 0;
				for(int x = -1; x<=1; x++)
				{		
					for(int y = -1; y<=1; y++)
					{		
						valor += sharpen[(x+1)+ size*(y+1)] *
					    umaImagem.getValordeUmPixel(i+x, y+j);
						//printf("%d", valor);
					}
				}
				
				valor /=div;
				
			
					
				valor= valor < 0 ? 0 : valor;
				valor=valor >255 ? 255 : valor;
				
				m[i+colunas*j] = valor;
			}
		}
		
		for(i=0;i<linhas; i++)
			for(j=0; j<colunas; j++)
				umaImagem.setValordeUmPixel(i, j, m[i+colunas*j]);

}

